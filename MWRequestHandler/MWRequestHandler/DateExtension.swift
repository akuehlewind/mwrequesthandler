//
//  DateExtension.swift
//  pon
//
//  Created by Adrian Kühlewind on 18.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

extension NSDate {
    public convenience init(json: String) {
        let parsed = NSDate.dateFromISOString(json)
        self.init(timeInterval: 0, sinceDate: parsed ?? NSDate())
    }
    public class func dateFromISOString(string: String) -> NSDate? {
        var date = isoDateTimeFormatter().dateFromString(string)
        if nil == date {
            date = isoDateTimeFormatterMilliseconds().dateFromString(string)
        }
        if nil == date {
            date = isoLocalDateTimeFormatter().dateFromString(string)
        }
        if nil == date {
            date = isoDateFormatter().dateFromString(string)
        }
        return date
    }
    public func isoDateString() -> String {
        return self.dynamicType.isoDateFormatter().stringFromDate(self)
    }
    public func isoDateTimeString() -> String {
        return self.dynamicType.isoDateTimeFormatter().stringFromDate(self)
    }
    // MARK: Date Formatter
    /**
    * Instantiates and returns an NSDateFormatter that understands ISO-8601 with timezone and milliseconds.
    */
    public class func isoDateTimeFormatterMilliseconds() -> NSDateFormatter {
        let formatter = NSDateFormatter() // class vars are not yet supported
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        return formatter
    }
    /**
    * Instantiates and returns an NSDateFormatter that understands ISO-8601 with timezone.
    */
    public class func isoDateTimeFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter() // class vars are not yet supported
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        return formatter
    }
    /**
    * Instantiates and returns an NSDateFormatter that understands ISO-8601 WITHOUT timezone.
    */
    public class func isoLocalDateTimeFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter() // class vars are not yet supported
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        return formatter
    }
    /**
    * Instantiates and returns an NSDateFormatter that understands ISO-8601 date only.
    */
    public class func isoDateFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter() // class vars are not yet supported
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
        return formatter
    }
}