//
//  RequestHandler.swift
//  pon
//
//  Created by Adrian Kühlewind on 17.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import CryptoSwift

protocol MWRequestHandlerDelegate {
    func requestHandler(handler: MWRequestHandler!, didFinishRequest data: NSDictionary?, identifier: String?, response: NSURLResponse?, info: NSDictionary?)
    func requestHandler(handler: MWRequestHandler!, didFailWithError error: MWRequestErrors!, identifier: String?, response: NSURLResponse?, info: NSDictionary?)
}

class MWRequestHandler : NSObject {
    var delegate: MWRequestHandlerDelegate?
    var config : MWRequestHandlerConfig = MWRequestHandlerConfig.sharedInstance()
    var requestUID : [String] = []
    
    override init(){
    }
    
    // Do request on service with id
    func request(id: String, info: NSDictionary?){
        let uid = ((NSUUID().UUIDString).lowercaseString)
        requestUID.append(uid)
        self.request(id, uid: uid, request: nil, retry: 30, info: info)
    }
    
    func request(id: String, request: MWRequestData?, info: NSDictionary?) {
        let uid = ((NSUUID().UUIDString).lowercaseString)
        requestUID.append(uid)
        self.setTimeout(request, uid: uid, info: info)
        self.request(id, uid: uid, request: request, retry: 30,info: info)
    }
    
    // Set timeout for request if set in request.timeout
    private func setTimeout(request: MWRequestData?, uid: String, info: NSDictionary?){
        // Add timeout settings for request and call delegate
        if (request != nil && request?.timeout != nil) {
            let timeout = dispatch_time(DISPATCH_TIME_NOW,Int64(request!.timeout! * Double(NSEC_PER_SEC)))
            dispatch_after(timeout, dispatch_get_main_queue()) {
                // Calls timeout if request in still on requestUID list
                if self.requestUID.contains(uid) {
                    self.requestUID.remove(uid)
                    self.callError(request, data: nil, response: nil, error: MWRequestErrors.Timeout, info: info)
                }
            }
        }
    }
    
    // Do request on service with id with meta data
    private func request(id: String, uid: String, request: MWRequestData?, retry: Int = 30, info: NSDictionary?){
        //var timeout : Double = 30
        
        if self.requestUID.contains(uid) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.doRequest(id, request: request) { (data, response, error) in
                    
                    //TODO: Refactoring queueing of requests. Is there a better way to delay and retry requests if deviceId is invalid or service list unavailable
                    if retry > 0 && error != nil && error == MWRequestErrors.Queue{
                        self.doRequestQueued(id, uid: uid, request: request, retry: retry-1, info: info)
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue()) {
                            if self.delegate != nil {
                                if self.requestUID.contains(uid) {
                                    self.requestUID.remove(uid)
                                    if error != nil {
                                        self.callError(request, data: data, response: response, error: error, info: info)
                                    }
                                    else{
                                        self.callSuccess(request, data: data, response: response, error: error, info: info)
                                    }
                                }
                            }
                        }
                        
                        
                    }
                }
            }
        }
    }
    
    private func callError(request: MWRequestData?, data: NSDictionary?, response: NSURLResponse?, error: MWRequestErrors?, info: NSDictionary?) {
        self.delegate?.requestHandler(self, didFailWithError: error, identifier: request?.identifier, response: response, info: info)
    }
    
    private func callSuccess(request: MWRequestData?, data: NSDictionary?, response: NSURLResponse?, error: MWRequestErrors?, info: NSDictionary?) {
        self.delegate?.requestHandler(self, didFinishRequest: data,  identifier: request?.identifier, response: response, info: info)
    }
    
    // Retry request delayed 1 second max 30 times
    private func doRequestQueued (id: String, uid: String, request: MWRequestData?, retry: Int, info: NSDictionary?) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,Int64(1 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
            self.request(id, uid: uid, request: request, retry: retry, info: info)
        }
    }
    
    private func doRequest(id: String, request: MWRequestData?, callback: (NSDictionary?, NSURLResponse?, MWRequestErrors?) -> ()) {
        
        // Check for network connection
        if isConnectedToNetwork() == false {
            callback(nil, nil, MWRequestErrors.NoInternet)
        }
        else{
            //Get list of available services
            getAvailableServices() { (services, error) in
                if self.config.servicesList.count == 0 {
                    // Error: Services list seems to be empty
                    callback (nil, nil, MWRequestErrors.ServicesListNotAvailable)
                }
                else
                {
                    // Next: Check if service exists in services list
                    self.getService(id, request: request) { (service, error) in
                        
                        if (service == nil) {
                            callback (nil, nil, error)
                        }
                        else
                        {
                            // Check if authorization is needed
                            if service.authorization == false {
                                // Next: Do Request - auth signature not required
                                self.getDataFromUrl(service, request: request) { (data, response, error) in
                                    callback(data, response, error)
                                }
                            }
                            else{
                                // Next: Check if device is already registered
                                self.registerDevice() { (deviceId, error) in
                                    if self.config.deviceId == nil || error != nil {
                                        callback(nil, nil, error)
                                    }
                                    else{
                                        // Update device data if necessary
                                        if (id != "device-update"){
                                            self.updateRegisteredDeviceData()
                                        }
                                        
                                        // Next: Do Request
                                        self.getDataFromUrl(service, request: request) { (data, response, error) in
                                            
                                            if response == nil || error != nil {
                                                callback(data,response,error)
                                            }
                                            else{
                                                let statusCode = (response as! NSHTTPURLResponse).statusCode
                                                if statusCode == 401 || statusCode == 403 {
                                                    let errorCodes = self.checkErrorCode(data)
                                                    
                                                    //Check for auth signature invalid
                                                    if (errorCodes.contains("KeyInvalid") || errorCodes.contains("SignatureMethodInvalid") || errorCodes.contains("KeyInvalidForGroup") || errorCodes.contains("TemporaryDisabled")){
                                                        callback(nil, nil, MWRequestErrors.AuthentificationFailure)
                                                    }
                                                    else if (errorCodes.contains("DeviceIdInvalid")) {
                                                        //TODO: Add retry and timestamp adjustment on expire failure
                                                        print("Device id has been recycled on server. Reset device id")
                                                        self.config.deviceId = nil
                                                        self.config.persistConfigData()
                                                        callback(nil, nil, MWRequestErrors.AuthentificationFailure)
                                                    }
                                                    else if (errorCodes.contains("KeyInvalid")) {
                                                        //TODO: Add retry and timestamp adjustment on expire failure
                                                        print("Should adjust timestamp and retry")
                                                        callback(nil, nil, MWRequestErrors.AuthentificationFailure)
                                                    }
                                                    else {
                                                        callback(data, response, error)
                                                    }
                                                }
                                                else if statusCode == 500 {
                                                    callback(data, response, MWRequestErrors.ServerError)
                                                }
                                                else{
                                                    callback(data, response, error)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func checkErrorCode (data: NSDictionary?) -> [String]{
        var err : [String] = []
        
        if data != nil  {
            if let errorsArray = data!["errors"] as? NSArray {
                for error in errorsArray {
                    if let errorsCode = error["code"] as? String {
                        err.append(errorsCode)
                    }
                }
            }
        }
        return err;
    }
    
    // Get service informations from servicesList
    private func getService(id: String, request: MWRequestData?, callback: (MWRequestService!, MWRequestErrors?) -> ()) {
        if let service = self.getServiceFromServiceList(id) {
            // Do URL Replacements if needed
            if request != nil && request?.replacements != nil && request?.replacements!.count > 0 {
                for replacement in request!.replacements! {
                    service.url = service.url.lowercaseString.stringByReplacingOccurrencesOfString(":\(replacement.0.lowercaseString)", withString: "\(replacement.1.lowercaseString)", options: NSStringCompareOptions.LiteralSearch, range: nil)
                }
            }
            callback(service,nil)
        }
        else
        {
            // Error: Service name not found in available services list
            callback(nil, MWRequestErrors.ServiceNotFound)
        }
    }
    
    // Check if service id exits in available services list
    func getServiceFromServiceList(id: String) -> MWRequestService? {
        let serviceList = self.config.servicesList.filter({( service: MWRequestService) -> Bool in
            return (id.lowercaseString == service.id.lowercaseString)
        })
        return serviceList.count > 0 ? serviceList[0].copy() as? MWRequestService : nil
    }
    
    // Update registered device data if has changed
    private func updateRegisteredDeviceData() {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let deviceData = self.collectDeviceData()
            let defaults = NSUserDefaults.standardUserDefaults()
            let registeredDeviceData = defaults.objectForKey("registeredDeviceData") as? [String:String]
            
            if registeredDeviceData! != deviceData {
                // Update registered device data
                let metaData = MWRequestData(
                    identifier: "UpdateRegisteredDevice",
                    data: deviceData, header: nil, params: nil, replacements: ["id" : self.config.deviceId!], timeout : 30)
                self.doRequest("device-update", request: metaData) { (data, response, error) -> () in
                    defaults.setObject(deviceData, forKey: "registeredDeviceData")
                }
            }
        }
    }
    
    // Collect device data for device register
    private func collectDeviceData() -> [String: String]{
        let appVersion = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as! String
        let appIdentifier = NSBundle.mainBundle().bundleIdentifier!
        let osVersion = UIDevice.currentDevice().systemVersion
        var group : String?
        let cultureCode = NSLocale.currentLocale().localeIdentifier.stringByReplacingOccurrencesOfString("_", withString: "-")
        
        switch UIDevice.currentDevice().userInterfaceIdiom {
        case .Phone:
            group = "phone"
        case .Pad:
            group = "tablet"
        default:
            group = "phone"
        }
        
        let deviceData = [
            "culture.code" : "\(cultureCode)",
            "app.id": appIdentifier,
            "app.version": appVersion,
            "device.os.type": "ios", //osName,
            "device.os.version": osVersion,
            "device.group":group!
        ]
        
        return deviceData
    }
    
    // Register device @middleware
    private func registerDevice(callback: (String?, MWRequestErrors?) -> ()) {
        if self.config.queueDeviceRegisterRequests == true {
            callback(nil, MWRequestErrors.Queue)
        }
        else{
            if self.config.deviceId != nil {
                callback(self.config.deviceId, nil)
            }
            else{
                self.config.queueDeviceRegisterRequests = true
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    
                    let deviceData = self.collectDeviceData()
                    
                    // Remember registered device data
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(deviceData, forKey: "registeredDeviceData")
                    
                    let metaData = MWRequestData(
                        identifier: "RegisterDevice",
                        data: deviceData, header: nil, params: nil, replacements: nil, timeout : 30)
                    self.doRequest("device-register", request: metaData) { (data, response, error) -> () in
                        if data != nil {
                            if data!["response"] != nil {
                                let dataArray = data!["response"] as! NSDictionary
                                self.config.deviceId = dataArray["id"] as? String
                                self.config.queueDeviceRegisterRequests = false
                            }
                        }
                        
                        if self.config.deviceId == nil {
                            self.config.queueDeviceRegisterRequests = false
                            callback(nil, MWRequestErrors.UnableToRegisterDevice)
                        }
                        else{
                            self.config.persistConfigData()
                            self.config.queueDeviceRegisterRequests = false
                            callback(self.config.deviceId, nil)
                        }
                    }
                }
            }
        }
        
        
    }
    
    // Get available services list from API services service
    private func getAvailableServices(callback: ([MWRequestService]?, MWRequestErrors?) -> ()) {
        
        var timeSinceLastServicesUpdate : Double = 0
        if config.servicesListExpireDate != nil {
            timeSinceLastServicesUpdate = abs(config.servicesListExpireDate!.timeIntervalSinceNow)
        }
        
        // Refresh services list by time
        if (self.config.servicesList.count > 0 && timeSinceLastServicesUpdate < (config.servicesListExpireInterval * 60) && config.apiServicesUrl == config.cachedApiServicesUrl){
            callback (self.config.servicesList, nil)
        }
        else{
            // Get Services from API
            let service = MWRequestService(id: "services", url: config.apiServicesUrl, modified: NSDate(timeIntervalSinceNow: 0), method: "GET", authorization: false)
            getDataFromUrl(service, request: nil) { (data, response, error) in
                if data != nil {
                    if data!.count > 0 {
                        self.config.servicesList = self.parseServices(data!)
                        self.config.servicesListExpireDate = NSDate()
                        self.config.cachedApiServicesUrl = self.config.apiServicesUrl
                        self.config.persistConfigData()
                        callback(self.config.servicesList, nil)
                    }
                }
                else
                {
                    // Error: Can not receive services list
                    callback(nil, MWRequestErrors.ServicesListNotAvailable)
                }
                
            }
        }
    }
    
    //Parse services list as MWRequestServices
    private func parseServices(json : AnyObject) -> [MWRequestService] {
        var servicesList : [MWRequestService] = []
        if let response = json["response"] as? NSArray {
            for item in response
            {
                let id = item["id"] as? String
                let method = item["method"] as? String
                let url = item["url"] as? String
                let modified = item["modified"] as? String
                let authorization = item["authorization"] as? Bool
                if (id != nil && method != nil && url != nil && modified != nil && authorization != nil) {
                    let service = MWRequestService(id: id!, url: url!, modified: NSDate(json: modified!), method: method!, authorization: authorization!)
                    servicesList.append(service)
                }
            }
        }
        return servicesList
    }
    
    // Generate auth signature token, requires CrytoSwift Framework
    private func generateAuthSignature(method: String, url: String, body : String?) -> String {
        // Signature parameters
        let deviceId = config.deviceId
        let secret = config.secret
        let secretId = config.secretId
        let timeStamp =  String(Int64(NSDate().timeIntervalSince1970))
        
        // Hash string for auth signature
        var stringToHash = "\(deviceId!)\(secret!)\(timeStamp)\(method)\(url)"
        
        // Add bodyData hash
        stringToHash += body != nil ? (body!.lowercaseString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!.sha256()?.toHexString())! : ""
        stringToHash = stringToHash.stringByReplacingOccurrencesOfString(" ", withString: "",options: NSStringCompareOptions.LiteralSearch, range: nil) // Bugfix for CryptoSwift returning spaces sometimes
        
        // Convert hash string to SHA256
        let signatureHash = ((stringToHash.lowercaseString).dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!.sha256()?.toHexString())!.lowercaseString
        var signatureToken = "\(deviceId!)\(secretId!)\(timeStamp)\(signatureHash)"
        signatureToken = signatureToken.stringByReplacingOccurrencesOfString(" ", withString: "",options: NSStringCompareOptions.LiteralSearch, range: nil) // Bugfix for CryptoSwift returning spaces sometimes
        return signatureToken
    }
    
    // URL Request
    private func getDataFromUrl(service : MWRequestService, request: MWRequestData?, callback: (NSDictionary?, NSURLResponse?, MWRequestErrors?) -> ()) {
        var requestURL = service.url
        
        // Add query parameters
        if request != nil && request?.params != nil && request?.params!.count > 0 {
            requestURL = "\(requestURL)?"
            var params = ""
            for field in request!.params! {
                if field.1 != "" && field.0 != "" {
                    // Use String Extension due to & and + are not encoded when using stringByAddingPercentEncodingWithAllowedCharacters
                    let key = (field.0).stringByAddingPercentEncodingForURLQueryValue()!
                    let value = (field.1).stringByAddingPercentEncodingForURLQueryValue()!
                    params = params == "" ? "\(key)=\((value))" : "\(params)&\(key)=\((value))"
                }
            }
            requestURL = "\(requestURL)\(params)"
        }
        
        let urlRequest = NSMutableURLRequest(URL: NSURL(string: requestURL)!)
        
        // Set header fields
        if request != nil && request?.header != nil && request?.header!.count > 0 {
            for field in request!.header! {
                if field.1 != "" && field.0 != "" {
                    urlRequest.setValue(field.1, forHTTPHeaderField: field.0)
                }
            }
        }
        
        // Add header field requestDate
        urlRequest.setValue("\(NSDate())", forHTTPHeaderField: "requestdate")
        
        // Add timeout settings for request
        if (request != nil && request?.timeout != nil) {
            urlRequest.timeoutInterval = request!.timeout!
        }else{
            urlRequest.timeoutInterval = 30
        }
        
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        urlRequest.HTTPMethod = service.method.uppercaseString
        
        var jsonErr = false
        var bodyDataJSON : NSData?
        var bodyJSONString : String?
        
        // Add request data to request
        if request != nil && request?.data != nil{
            let bodyData : AnyObject! = request?.data!
            if NSJSONSerialization.isValidJSONObject(bodyData) {
                do {
                    bodyDataJSON = try NSJSONSerialization.dataWithJSONObject(bodyData, options: NSJSONWritingOptions())
                    bodyJSONString = NSString(data: bodyDataJSON!, encoding: NSUTF8StringEncoding)! as String
                } catch {
                    jsonErr = true
                }
            }
            else{
                jsonErr = true
            }
        }
        
        if (jsonErr == true) {
            callback(nil, nil, MWRequestErrors.JsonSerializationError)
        }
        else{
            // Thow error if get method and body data set
            if bodyDataJSON != nil && urlRequest.HTTPMethod == "GET" {
                callback(nil, nil, MWRequestErrors.BodyNotAllowedForMethod)
            }
            else{
                if bodyDataJSON != nil
                {
                    urlRequest.HTTPBody = bodyDataJSON
                }
                
                // Generate authorization signature
                if service.authorization == true {
                    let authSignature = generateAuthSignature(service.method, url: requestURL, body: bodyJSONString)
                    // Set signature header
                    urlRequest.setValue(authSignature, forHTTPHeaderField: "key")
                }
                
                // Run data task
                let session = NSURLSession.sharedSession()
                let task = session.dataTaskWithRequest(urlRequest, completionHandler: {data, response, error -> Void in
                    if(error != nil) {
                        // Error: An error occured while requesting url
                        callback(nil, response, MWRequestErrors.RequestError)
                    }
                    else
                    {
                        if response == nil {
                            callback(nil, nil,MWRequestErrors.EmptyResponse)
                        }
                        else{
                            if data!.length == 0  {
                                callback(nil, response, nil)
                            }
                            else{
                                // Parse response to JSON
                                do {
                                    let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                                    // Get server date and calculate time difference
                                    if service.authorization == true {
                                        
                                        let serverDate: String? = (response as! NSHTTPURLResponse).allHeaderFields["Date"] as? String
                                        if serverDate != nil {
                                            //TODO: Calculate server time offset and adjust client time of authorisation fails
                                            //println(serverDate)
                                        }
                                    }
                                    callback(jsonResult, response, nil)
                                    
                                    
                                } catch {
                                    callback(nil, response, MWRequestErrors.JSONParseError)
                                }
                            }
                        }
                    }
                    
                })
                
                task.resume()
            }
        }
        
    }
    
    private func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags.ConnectionAutomatic
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
