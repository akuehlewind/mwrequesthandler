//
//  RequestHandlerConfig.swift
//  pon
//
//  Created by Adrian Kühlewind on 17.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

class MWRequestHandlerConfig : NSObject {
    
    class func sharedInstance() -> MWRequestHandlerConfig! {
        struct Static {
            static var instance: MWRequestHandlerConfig? = nil
            static var onceToken: dispatch_once_t = 0
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = self.init()
        }
        return Static.instance!
    }
    
    override required init(){
        super.init()
        getConfigData()
    }
    
    // ******** Middleware Api Config ************************
    var apiServicesUrl = "https://xxxxxxxx"
    var secret : String? = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    var secretId : String? = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    var servicesListExpireInterval : Double = 1440 // in minutes
    // *******************************************************
    
    var servicesList : [MWRequestService] = []
    var deviceId : String?
    var servicesListExpireDate : NSDate?
    var queueSessionRequests = false
    var queueDeviceRegisterRequests : Bool = false
    var cachedApiServicesUrl: String?
    
    // Persist variables to disk
    internal func persistConfigData () {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(servicesList), forKey: "servicesList")
        defaults.setValue(deviceId, forKey: "deviceId")
        defaults.setValue(servicesListExpireDate, forKey: "servicesListExpireDate")
        defaults.setValue(apiServicesUrl, forKey: "apiServicesUrl")
    }
    
    // Load variables from disk
    internal func getConfigData () {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        deviceId=defaults.stringForKey("deviceId")
        servicesListExpireDate=defaults.objectForKey("servicesListExpireDate") as? NSDate
        cachedApiServicesUrl=defaults.stringForKey("apiServicesUrl")
        if let servicesListData = defaults.objectForKey("servicesList") as? NSData {
            servicesList = NSKeyedUnarchiver.unarchiveObjectWithData(servicesListData) as! [MWRequestService]
        }
        
    }
    
}
