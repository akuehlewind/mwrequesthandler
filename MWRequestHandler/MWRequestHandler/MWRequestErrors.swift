//
//  ApiErrors.swift
//  pon
//
//  Created by Adrian Kühlewind on 17.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

enum MWRequestErrors : Int32 {
    
    // No connection to network
    case NoInternet
    
    // Can not load services list from api
    case ServicesListNotAvailable
    
    // Service not found in services list
    case ServiceNotFound
    
    //DeviceId is not valid. Maybe got recyled in api db. Try generating a new one and retry
    case DeviceIdInvalid
    
    // Authentification not possible
    case AuthentificationFailure
    
    // An error occured during the request
    case RequestError
    
    // Response is not a valid JSON doucment. Error while parsing JSON
    case JSONParseError
    
    // Can not register device using device register api service.
    case UnableToRegisterDevice
    
    // Response is empty or no connection to service
    case EmptyResponse
    
    // Request can not be executed due to missing requirements (i.e. deviceId, sessionId). Retry later or queue
    case Queue
    
    // Request data can not be serialized
    case JsonSerializationError
    
    // Body data set but request method is GET. This is not possible
    case BodyNotAllowedForMethod
    
    // Timeout reached
    case Timeout
    
    // Server returned 500 as http code
    case ServerError
}
