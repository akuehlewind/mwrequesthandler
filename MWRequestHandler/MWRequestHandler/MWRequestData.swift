//
//  ApiRequest.swift
//  pon
//
//  Created by Adrian Kühlewind on 17.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

// Meta data
struct MWRequestData {
    var identifier: String?
    var data : AnyObject?
    var header : [String:String]?
    var params : [String:String]?
    var replacements : [String:String]?
    var timeout: Double? = 30
}