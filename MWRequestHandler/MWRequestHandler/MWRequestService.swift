//
//  ApiService.swift
//  pon
//
//  Created by Adrian Kühlewind on 17.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

class MWRequestService : NSObject, NSCoding, NSCopying{
    var id: String!
    var name : String!
    var url : String!
    var modified : NSDate!
    var method : String!
    var authorization : Bool!
    
    override init() {
    }
    
    convenience init(id: String, url: String, modified: NSDate, method: String, authorization: Bool){
        self.init()
        self.id = id
        self.url = url
        self.modified = modified
        self.method = method
        self.authorization = authorization
    }
    
    //NSCoding
    required convenience init?(coder decoder: NSCoder) {
        self.init()
        self.id = decoder.decodeObjectForKey("id") as! String
        self.url = decoder.decodeObjectForKey("url") as! String
        self.modified = decoder.decodeObjectForKey("modified") as! NSDate
        self.method = decoder.decodeObjectForKey("method") as! String
        self.authorization = decoder.decodeBoolForKey("authorization")
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.url, forKey: "url")
        coder.encodeObject(self.modified, forKey: "modified")
        coder.encodeObject(self.method, forKey: "method")
        coder.encodeBool(self.authorization, forKey: "authorization")
    }
    
    func copyWithZone(zone: NSZone) -> AnyObject {
        let newValue = MWRequestService()
        newValue.id = id
        newValue.url = url
        newValue.modified = modified
        newValue.method = method
        newValue.authorization = authorization
        return newValue
    }
}
