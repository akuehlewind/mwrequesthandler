//
//  StringExtension.swift
//  pon
//
//  Created by Adrian Kühlewind on 10.01.15.
//  Copyright (c) 2015 Adrian Kühlewind. All rights reserved.
//

import Foundation

extension String {
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumericCharacterSet()
        characterSet.addCharactersInString("-._~")
        
        return stringByAddingPercentEncodingWithAllowedCharacters(characterSet)
    }
}

