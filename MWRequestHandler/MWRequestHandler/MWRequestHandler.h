//
//  MWRequestHandler.h
//  MWRequestHandler
//
//  Created by Adrian Kühlewind on 21.01.16.
//  Copyright © 2016 Adrian Kühlewind. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MWRequestHandler.
FOUNDATION_EXPORT double MWRequestHandlerVersionNumber;

//! Project version string for MWRequestHandler.
FOUNDATION_EXPORT const unsigned char MWRequestHandlerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MWRequestHandler/PublicHeader.h>


